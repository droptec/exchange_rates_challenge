package internal

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client"
	"context"
	"fmt"
	"github.com/go-chi/chi"

	"bitbucket.org/droptec/exchange_rates_challenge/internal/handler"
	"bitbucket.org/droptec/exchange_rates_challenge/internal/httptrans"
	"bitbucket.org/droptec/exchange_rates_challenge/internal/service"
	"github.com/rs/zerolog"
	"net"
	"net/http"
	"time"
)

var (
	exchangeRatesClient exchange_rates_client.Client
	httpRouter          chi.Router
	listenAddress       string

	shutdownCh <-chan struct{}
)

func Init(client exchange_rates_client.Client, listenAddr string, logger zerolog.Logger) () {
	// init locals
	exchangeRatesClient = client

	// set up the router
	listenAddress = listenAddr
	httpRouter = chi.NewRouter()
	httptrans.Init(httpRouter, time.Second*10, logger)

	// create the services
	compareRatesService := service.NewCompareRatesService(client)

	// set up the handlers
	handlers := []handler.Handler{
		handler.NewCompareRatesEuro(compareRatesService, exchangeRatesClient, logger),
	}
	handler.Bind(httpRouter, handlers...)
}

func Start() {
	httpServer := &http.Server{
		Addr:    listenAddress,
		Handler: httpRouter,
	}
	startFuncErrCh := make(chan error)
	startFunc := func() {
		listener, err := net.Listen("tcp", listenAddress)
		if err != nil {
			startFuncErrCh <- err
			return
		}
		httpServer.Serve(listener)
	}

	stopFunc := func() {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		httpServer.Shutdown(ctx)
		fmt.Print("Shutdown finished")
	}

	go startFunc()

	select {
	case <-shutdownCh:
		stopFunc()
	}
}
