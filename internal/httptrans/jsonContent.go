package httptrans

import (
	"fmt"
	"net/http"
	"strings"
)

// If the Content-Type header is not set to application/json return a 400
func WithJSONContent(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {

			if r.Method == "POST" || r.Method == "PUT" || r.Method == "PATCH" || r.Method == "DELETE" {
				types := strings.Split(r.Header.Get("Content-Type"), ";")

				var found bool
				for _, ct := range types {
					if ct == "application/json" {
						found = true
					}
				}

				err := fmt.Errorf("expected application/json content type")
				if !found {
					SendError(err, 400, w, r)
					return
				}
			}

			next.ServeHTTP(w, r)
		})

}
