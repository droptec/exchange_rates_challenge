package httptrans

import (
	"encoding/json"
	"net/http"
)

func SendError(err error, code int, w http.ResponseWriter, r *http.Request) {
	logger.Error().Interface("err", err).Msgf("Error returned as a response err:%v", err.Error())

	// Send HTTP Response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	response, err := json.Marshal(err.Error())
	if err != nil {
		logger.Error().Interface("err", err).Msgf("could not marshal error, err:%v", err.Error())
	}

	w.Write(response)
}
