package httptrans

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/rs/zerolog"
)

type createdResponse struct {
	Links map[string]string `json:"links"`
}

type listResponse struct {
	Data   interface{} `json:"data"`
	Count  int         `json:"count"`
	Total  int         `json:"total"`
	Offset int         `json:"offset,omitempty"`
	Limit  int         `json:"limit,omitempty"`
}

func SendJSONListWithLimitOffset(data interface{}, total, count, offset, limit int, w http.ResponseWriter, r *http.Request) {
	SendJSON(map[string]interface{}{
		"data":   data,
		"total":  total,
		"count":  count,
		"limit":  limit,
		"offset": offset,
	}, w, r)
}

func SendJSONList(data interface{}, total int, count int, w http.ResponseWriter, r *http.Request) {
	SendJSON(map[string]interface{}{
		"data":  data,
		"total": total,
		"count": count,
	}, w, r)
}

func SendJSON(data interface{}, w http.ResponseWriter, r *http.Request) {
	SendJSONWithStatus(data, http.StatusOK, w, r)
}

func SendJSONWithStatus(data interface{}, status int, w http.ResponseWriter, r *http.Request) {
	logger := zerolog.Ctx(r.Context())
	bytes, err := json.Marshal(data)
	if err != nil {
		logger.
			Fatal().
			Interface("data", data).
			Err(err).
			Msg("could not encode json http response")
		err2 := fmt.Errorf("could not encode json http response")
		SendError(err2, http.StatusBadRequest, w, r)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(bytes)
}
