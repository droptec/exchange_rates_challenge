package httptrans

import (
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/rs/zerolog"
)

var logger zerolog.Logger

func Init(router chi.Router, httpTimeout time.Duration, log zerolog.Logger) {
	router.Use(middleware.Timeout(httpTimeout))
	router.Use(middleware.StripSlashes)
	router.Use(withCorrelationID)
	router.Use(withLogger(log))
	router.Use(recoverer)
	router.Use(withCORS)

	logger = log
}
