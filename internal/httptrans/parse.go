package httptrans

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/tomwright/chiuriattr"
	"github.com/tomwright/queryparam"
)

// ParseRequest parses all parts of the given request
func ParseRequest(r *http.Request, req interface{}) error {
	if err := ParseRequestURL(r, req); err != nil {
		return err
	}
	if err := ParseRequestJSON(r, req); err != nil {
		return err
	}
	return nil
}

// ParseRequestURL parses all parts of the given request that relate to the URL
func ParseRequestURL(r *http.Request, req interface{}) error {
	if err := ParseRequestQuery(r, req); err != nil {
		return err
	}
	if err := ParseRequestURI(r, req); err != nil {
		return err
	}
	return nil
}

func ParseRequestQuery(r *http.Request, req interface{}) error {
	err := chiuriattr.Unmarshal(r, req)
	if err != nil {
		return fmt.Errorf("could not decode request query attributes: " + err.Error())
	}
	return nil
}

func ParseRequestURI(r *http.Request, req interface{}) error {
	err := queryparam.Unmarshal(r.URL, req)
	if err != nil {
		return fmt.Errorf("could not decode request uri:" + err.Error())
	}
	return nil
}

func ParseRequestJSON(r *http.Request, req interface{}) error {
	if strings.HasPrefix(r.Header.Get("Content-Type"), "application/json") {
		bytes, err := GetRequestBody(r)
		if err != nil {
			return err
		}
		err = ParseJSONFromBytes(bytes, req)
		if err != nil {
			return err
		}
	}

	return nil
}

func ParseRequestJSONMultiple(r *http.Request, req ...interface{}) error {
	if strings.HasPrefix(r.Header.Get("Content-Type"), "application/json") {
		bytes, err := GetRequestBody(r)
		if err != nil {
			return err
		}

		for _, re := range req {
			err = ParseJSONFromBytes(bytes, re)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func GetRequestBody(r *http.Request) ([]byte, error) {
	bytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, fmt.Errorf("could not read request body: " + err.Error())
	}
	return bytes, nil
}

func ParseJSONFromBytes(bytes []byte, req interface{}) error {
	err := json.Unmarshal(bytes, req)
	if err != nil {
		return fmt.Errorf("could not decode request body:" + err.Error())
	}

	return nil
}
