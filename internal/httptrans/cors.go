package httptrans

import "net/http"

// Set up CORS headers
func withCORS(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			origin := r.Header.Get("Origin")
			if origin == "" {
				origin = "*"
			}
			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PATCH, PUT, DELETE, OPTIONS, TRACE, HEAD")
			w.Header().Set("Access-Control-Allow-Headers", "X-Platform-UUID,X-User-UUID,X-Tenant-UUID,Content-Type,Content-Length,Accept,Authorization,If-Match")
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.Header().Set("Access-Control-Expose-Headers", "Etag")
			w.Header().Set("Cache", "Authorization, Origin, Content-Type")
			w.Header().Set("Cache-Control", "max-age=0, no-cache, no-store, must-revalidate")
			w.Header().Set("Pragma", "no-cache")
			w.Header().Set("Expires", "-1")

			if r.Method == "OPTIONS" {
				return
			}

			next.ServeHTTP(w, r)
		})
}
