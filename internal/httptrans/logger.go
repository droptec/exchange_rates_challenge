package httptrans

import (
	"net/http"

	"github.com/rs/zerolog"
)

func withLogger(logger zerolog.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(
			func(w http.ResponseWriter, r *http.Request) {
				rLogger := logger
				correlationID := r.Context().Value("CORRELATION_ID")
				if correlationID != nil {
					rLogger = rLogger.With().
						Str("correlationId", correlationID.(string)).
						Logger()
				}

				ctx := rLogger.WithContext(r.Context())

				next.ServeHTTP(w, r.WithContext(ctx))
			})
	}
}
