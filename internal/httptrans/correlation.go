package httptrans

import (
	"context"
	"net/http"

	"github.com/satori/go.uuid"
)

func CorrelationIDFromContext(ctx context.Context) string {
	c := ctx.Value("CORRELATION_ID")
	if correlationID, ok := c.(string); ok {
		return correlationID
	}
	return ""
}

func withCorrelationID(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {

			correlationId := r.Header.Get("X-Correlation-UUID")
			if correlationId == "" {
				id, _ := uuid.NewV4()
				correlationId = id.String()
			}

			newCtx := context.WithValue(r.Context(), "CORRELATION_ID", correlationId)

			next.ServeHTTP(w, r.WithContext(newCtx))
		})
}
