package httptrans

import (
	"net/http"
	"runtime/debug"
	"strings"

	"github.com/rs/zerolog"
)

// Recover from panics and send a 500 response
func recoverer(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				if rec := recover(); rec != nil {
					// Print error and stacktrace into stderr
					stacktrace := strings.Replace(string(debug.Stack()), "\n", " | ", -1)

					zerolog.Ctx(r.Context()).
						Error().
						Interface("rec", rec).
						Str("stacktrace", stacktrace).
						Msg("Uncaught panic caught by HTTP recoverer")

					// Send HTTP 500 response
					w.Header().Set("Content-Type", "application/json")
					w.WriteHeader(500)
					message := `{"message":"Internal Server Error"}`
					w.Write([]byte(message))
					return
				}
			}()

			next.ServeHTTP(w, r)
		})

}
