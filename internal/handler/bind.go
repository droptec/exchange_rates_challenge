package handler

import (
	"github.com/go-chi/chi"
)

func Bind(router chi.Router, handlers ...Handler) {
	for _, h := range handlers {
		if httpHandler, ok := h.(HTTPHandler); ok {
			httpHandler.BindHTTP(router)
		}
		if rpcHandler, ok := h.(RPCHandler); ok {
			rpcHandler.BindRPC()
		}
	}
}
