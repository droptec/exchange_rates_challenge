package handler

import "github.com/go-chi/chi"

type Handler interface {
}

type HTTPHandler interface {
	BindHTTP(router chi.Router)
}

type RPCHandler interface {
	BindRPC()
}
