package handler

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client"
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
	"bitbucket.org/droptec/exchange_rates_challenge/internal/httptrans"
	"bitbucket.org/droptec/exchange_rates_challenge/internal/service"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/rs/zerolog"
	"net/http"
)

var ComparisonCurrencyDefault = "EUR"

func NewCompareRatesEuro(compareRatesService service.CompareRatesService, exchangeClient exchange_rates_client.Client, logger zerolog.Logger, ) *compareRates {
	return &compareRates{
		CompareRatesService: compareRatesService,
		ExchangeClient:      exchangeClient,
		logger:              logger,
	}
}

type compareRates struct {
	CompareRatesService service.CompareRatesService
	ExchangeClient      exchange_rates_client.Client
	logger              zerolog.Logger
}

func (compareRates *compareRates) BindHTTP(router chi.Router) {
	router.Group(func(r chi.Router) {
		r.Get("/v1/compare-rates-euro/{base_rate}", compareRates.HandleHTTP)
	})
}

type compareRatesV1HTTPData struct {
	ComparisonCurrency string `json:"currency"`
	BaseCurrency       string `json:"-" uriattr:"base_rate"`
}

func (compareRates *compareRates) HandleHTTP(rw http.ResponseWriter, r *http.Request) {
	data := &compareRatesV1HTTPData{}
	if errP := httptrans.ParseRequest(r, data); errP != nil {
		err := fmt.Errorf("error handling request data, please check you have sent correct data, err details : %v", errP.Error())
		httptrans.SendError(err, http.StatusBadRequest, rw, r)
		return
	}

	// extending this endpoint to work with other comparison currencies will be easy as this data.BaseCurrency just needs updating
	data.ComparisonCurrency = ComparisonCurrencyDefault

	err := validate(*data, rw, r)
	if err != nil {
		httptrans.SendError(err, http.StatusBadRequest, rw, r)
		return
	}

	baseCurrency, comparisonCurrency := domain.Currency(data.BaseCurrency), domain.Currency(data.ComparisonCurrency)
	response, err := compareRates.CompareRatesService.CompareRates(baseCurrency, comparisonCurrency)
	if err != nil {
		compareRates.logger.Error().Interface("err", err).Msg("Error comparing rates, err: " + err.Error())
		httptrans.SendError(err, http.StatusInternalServerError, rw, r)
		return
	}

	httptrans.SendJSONWithStatus(&response, http.StatusAccepted, rw, r)
}

// todo: (short on time!) this function should be in a separate Validator file and use domain rules to validate each bit
func validate(data compareRatesV1HTTPData, rw http.ResponseWriter, r *http.Request) error {

	if data.BaseCurrency == "" || data.ComparisonCurrency == "" {
		err := fmt.Errorf("error, base or comparison currency was empty")
		return err
	}

	if data.BaseCurrency == data.ComparisonCurrency {
		err := fmt.Errorf("error: base and comparison currencies were the same, please use difference base currency")
		return err
	}

	// assert currency type
	baseCurrency, err := domain.GetCurrencyFromString(data.BaseCurrency)
	if err != nil {
		return fmt.Errorf("given input for base_currency (%v) was not a valid curency, please note the currency should be upper case", data.BaseCurrency)
	}
	data.ComparisonCurrency = string(baseCurrency)

	comparisonCurrency, err := domain.GetCurrencyFromString(data.ComparisonCurrency)
	if err != nil {
		return fmt.Errorf("given input for comparison currency (%v) was not a valid curency, please note the currency should be upper case", data.ComparisonCurrency)
	}
	data.ComparisonCurrency = string(comparisonCurrency)

	return nil
}
