package service

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client"
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
	Internal "bitbucket.org/droptec/exchange_rates_challenge/v1/domain"
	"fmt"
	"strconv"
)

type ratesService struct {
	exchangeClient exchange_rates_client.Client
}

type CompareRatesService interface {
	CompareRates(base, comparison domain.Currency) (*Internal.CompareRatesResponse, error)
}

func NewCompareRatesService(exchangeClient exchange_rates_client.Client) CompareRatesService {
	return ratesService{
		exchangeClient: exchangeClient,
	}
}

func (ratesService ratesService) CompareRates(baseCurrency, comparisonCurrency domain.Currency) (*Internal.CompareRatesResponse, error) {
	rates, err := ratesService.exchangeClient.GetLatestRates(baseCurrency)
	if err != nil {
		return nil, fmt.Errorf("service compareRates could not get latest rates from exchange Client, err:%v", err.Error())
	}

	price, ok := rates[comparisonCurrency]
	if !ok {
		return nil, fmt.Errorf("no price was found for the given comparison")
	}

	recommendation, err := ratesService.RecommendationBasedOnCurrentPrice(baseCurrency, comparisonCurrency, price)
	if err != nil {
		return nil, fmt.Errorf("when comparing Rate could not get recommendation, err: %v", err.Error())
	}

	priceStr := strconv.FormatFloat(float64(price), 'f', 2, 64)
	result := fmt.Sprintf("One %v is worth %v %v", baseCurrency, priceStr, comparisonCurrency)
	response := Internal.CompareRatesResponse{
		BaseCurrency:       baseCurrency,
		ComparisonCurrency: comparisonCurrency,
		Price:              price,
		Recommendation:     recommendation,
		Rate:               result,
	}

	return &response, nil
}
