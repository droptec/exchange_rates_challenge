package service

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client"
	ClientDomain "bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
	"bitbucket.org/droptec/exchange_rates_challenge/v1/domain"
	"fmt"
	"github.com/stretchr/testify/mock"
	"reflect"
	"testing"
)

type exchangeRatesClientMock struct {
	mock.Mock
}

type exchangeRatesClientMockPricesHigher struct {
	mock.Mock
}

type exchangeRatesClientMockPricesLower struct {
	mock.Mock
}

var TestRates ClientDomain.Rates
var TestRatesLower ClientDomain.Rates
var TestRatesHigher ClientDomain.Rates

func NewCompareRatesServiceMock(exchangeClient exchange_rates_client.Client) CompareRatesService {
	return ratesService{
		exchangeClient: exchangeClient,
	}
}

type TestStruct struct {
	name                string
	Expected            *domain.CompareRatesResponse
	CompareRatesService CompareRatesService
	Result              string
	ShouldPass          bool
}

func TestCompareRates(t *testing.T) {
	TestRates = make(ClientDomain.Rates, 0)
	TestRates["GBP"] = 0.8
	TestRates["USD"] = 1.2

	TestRatesLower = make(ClientDomain.Rates, 0)
	TestRatesLower["GBP"] = 0.7
	TestRatesLower["USD"] = 1.1

	TestRatesHigher = make(ClientDomain.Rates, 0)
	TestRatesHigher["GBP"] = 0.9
	TestRatesHigher["USD"] = 1.3

	// mocking the exchange client
	// Same Prices
	exchangeRatesClientMockTestRates := exchangeRatesClientMock{}
	exchangeRatesClientMockTestRates.On("GetLatestRates", mock.Anything).Return(TestRates, nil)
	exchangeRatesClientMockTestRates.On("GetRatesForPrevDays", mock.Anything, mock.Anything).Return(TestRates, nil)
	ratesServiceMockTestRates := NewCompareRatesServiceMock(exchangeRatesClientMockTestRates)

	// Higher Prices
	exchangeRatesClientMockTestRatesLower := exchangeRatesClientMockPricesHigher{}
	exchangeRatesClientMockTestRatesLower.On("GetLatestRates", mock.Anything).Return(TestRatesLower, nil)
	exchangeRatesClientMockTestRatesLower.On("GetRatesForPrevDays", mock.Anything, mock.Anything).Return(TestRates, nil)
	ratesServiceMockTestRatesLower := NewCompareRatesServiceMock(exchangeRatesClientMockTestRatesLower)

	// Lower Prices
	exchangeRatesClientMockTestRatesHigher := exchangeRatesClientMockPricesLower{}
	exchangeRatesClientMockTestRatesHigher.On("GetLatestRates", mock.Anything).Return(TestRatesHigher, nil)
	exchangeRatesClientMockTestRatesHigher.On("GetRatesForPrevDays", mock.Anything, mock.Anything).Return(TestRates, nil)
	ratesServiceMockTestRatesHigher := NewCompareRatesServiceMock(exchangeRatesClientMockTestRatesHigher)

	t1 := TestStruct{
		name:                "test 1 - no change in prices GBP",
		CompareRatesService: ratesServiceMockTestRates,
		Expected: &domain.CompareRatesResponse{
			BaseCurrency:       ClientDomain.Currency("EUR"),
			ComparisonCurrency: ClientDomain.Currency("GBP"),
			Price:              ClientDomain.Price(0.8),
			Rate:               "One EUR is worth 0.80 GBP",
			Recommendation:     RecommendationForPriceTheSame,
		},
		ShouldPass: true,
	}
	tests := []TestStruct{t1}

	t2 := TestStruct{
		name:                "test 2 - price decreasing USD",
		CompareRatesService: ratesServiceMockTestRatesLower,
		Expected: &domain.CompareRatesResponse{
			BaseCurrency:       ClientDomain.Currency("EUR"),
			ComparisonCurrency: ClientDomain.Currency("USD"),
			Price:              ClientDomain.Price(1.1),
			Rate:               "One EUR is worth 1.10 USD",
			Recommendation:     RecommendationForPriceDecreasing,
		},
		ShouldPass: true,
	}
	tests = append(tests, t2)

	t3 := TestStruct{
		name:                "test 3 - price increasing USD",
		CompareRatesService: ratesServiceMockTestRatesHigher,
		Expected: &domain.CompareRatesResponse{
			BaseCurrency:       ClientDomain.Currency("EUR"),
			ComparisonCurrency: ClientDomain.Currency("USD"),
			Price:              ClientDomain.Price(1.3),
			Rate:             "One EUR is worth 1.30 USD",
			Recommendation:     RecommendationForPriceIncreasing,
		},
		ShouldPass: true,
	}
	tests = append(tests, t3)

	t4 := TestStruct{
		name:                "test 4 - price increasing USD but expected false positive.",
		CompareRatesService: ratesServiceMockTestRatesHigher,
		Expected: &domain.CompareRatesResponse{
			BaseCurrency:       ClientDomain.Currency("EUR"),
			ComparisonCurrency: ClientDomain.Currency("USD"),
			Price:              ClientDomain.Price(1.30),
			Rate:             "One EUR is worth 1.30 USD",
			Recommendation:     RecommendationForPriceDecreasing,
		},
		ShouldPass: false,
	}
	tests = append(tests, t4)

	// just doing three basic tests
	for _, test := range tests {
		actual, err := test.CompareRatesService.CompareRates(test.Expected.BaseCurrency, test.Expected.ComparisonCurrency)
		if test.ShouldPass && err != nil {
			t.Errorf("error on test (%v), err: %v", test.name, err.Error())
		}
		if test.ShouldPass && !reflect.DeepEqual(actual, test.Expected) {
			t.Errorf("error on test (%v) \n expected (%v), \n but got (%v)", test.name, test.Expected, actual)
		}
	}

}

// Mock functions returning dumby data for each type of test

func (m exchangeRatesClientMock) GetLatestRates(baseCurrency ClientDomain.Currency) (ClientDomain.Rates, error) {
	args := m.Called(baseCurrency)
	if arg, ok := args.Get(0).(ClientDomain.Rates); ok {
		return arg, args.Error(1)
	}
	return nil, fmt.Errorf("GetLatestRates() mock was not expecting this function call")
}

func (m exchangeRatesClientMock) GetRatesForPrevDays(days int, baseCurrency ClientDomain.Currency) (ClientDomain.Rates, error) {
	args := m.Called(days, baseCurrency)
	if arg, ok := args.Get(0).(ClientDomain.Rates); ok {
		return arg, args.Error(1)
	}
	return nil, fmt.Errorf("GetRatesForPrevDays() mock was not expecting this function call")
}

func (m exchangeRatesClientMockPricesHigher) GetLatestRates(baseCurrency ClientDomain.Currency) (ClientDomain.Rates, error) {
	args := m.Called(baseCurrency)
	if arg, ok := args.Get(0).(ClientDomain.Rates); ok {
		return arg, args.Error(1)
	}
	return nil, fmt.Errorf("GetLatestRates() mock was not expecting this function call")
}

func (m exchangeRatesClientMockPricesHigher) GetRatesForPrevDays(days int, baseCurrency ClientDomain.Currency) (ClientDomain.Rates, error) {
	args := m.Called(days, baseCurrency)
	if arg, ok := args.Get(0).(ClientDomain.Rates); ok {
		return arg, args.Error(1)
	}
	return nil, fmt.Errorf("GetRatesForPrevDays() mock was not expecting this function call")
}

func (m exchangeRatesClientMockPricesLower) GetLatestRates(baseCurrency ClientDomain.Currency) (ClientDomain.Rates, error) {
	args := m.Called(baseCurrency)
	if arg, ok := args.Get(0).(ClientDomain.Rates); ok {
		return arg, args.Error(1)
	}
	return nil, fmt.Errorf("GetLatestRates() mock was not expecting this function call")
}

func (m exchangeRatesClientMockPricesLower) GetRatesForPrevDays(days int, baseCurrency ClientDomain.Currency) (ClientDomain.Rates, error) {
	args := m.Called(days, baseCurrency)
	if arg, ok := args.Get(0).(ClientDomain.Rates); ok {
		return arg, args.Error(1)
	}
	return nil, fmt.Errorf("GetRatesForPrevDays() mock was not expecting this function call")
}
