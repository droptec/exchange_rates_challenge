package service

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
	Internal "bitbucket.org/droptec/exchange_rates_challenge/v1/domain"
	"fmt"
)

const (
	RecommendationForPriceIncreasing = "the price is rising, time to buy and ride that wave!"
	RecommendationForPriceDecreasing = "the price is tanking, time to sell if you have any"
	RecommendationForPriceTheSame    = "the price has not moved since last week, hold fire on all decisions"
)

func (ratesService ratesService) RecommendationBasedOnCurrentPrice(baseCurrency, compareCurrency domain.Currency, priceCurrent domain.Price) (string, error) {
	ratesLastWeek, err := ratesService.exchangeClient.GetRatesForPrevDays(7, baseCurrency)
	if err != nil {
		return "", fmt.Errorf("while recommending, rates could not be fetched, err:%v", err.Error())
	}
	priceLastWeek, ok := ratesLastWeek[compareCurrency]
	if !ok {
		return "", fmt.Errorf("while recommending, rates for currency (%v) could not be found", compareCurrency)
	}

	recommendationData := Internal.RecommendationData{
		PriceLastWeek: priceLastWeek,
		PriceCurrent:  priceCurrent,
	}
	recommendation := recommendationBasedOnCurrentPrice(recommendationData)

	return recommendation, nil
}

func recommendationBasedOnCurrentPrice(data Internal.RecommendationData) string {
	if data.PriceLastWeek < data.PriceCurrent {
		return RecommendationForPriceIncreasing
	}

	if data.PriceCurrent < data.PriceLastWeek {
		// tanking is potentially exaggeration, so i would add more logic here and depending on % difference would swap 'tanking' for more appropriate descriptions
		return RecommendationForPriceDecreasing
	}

	return RecommendationForPriceTheSame
}
