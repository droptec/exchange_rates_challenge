# Exchange Rates Challenge

This app will work with go 1.12 and above.

## Prerequisites
Git
Go (at least 1.12)
Port 80 is free (check apache is not running on this port)

## Installing
How to install on Ubuntu and unix based operating systems :
Shell in to the server's terminal where you want this app to run

1. Make sure you have git installed
2. Make sure you have Go installed (at least 1.12)
3. Create the following folder: $GOPATH/bitbucket.org/droptec/
4. Naviagate to the droptec folder within your terminal then copy the repo with the following command
$ git clone https://droptec@bitbucket.org/droptec/exchange_rates_challenge.git

5. In your terminal navigate to /bitbucket.org/droptec/exchange_rates_challenge/cmd/app

6. Run the following commands command:
$ go get github.com/go-chi/chi
$ go get github.com/rs/zerolog
$ go get github.com/satori/go.uuid
$ go get github.com/tomwright/chiuriattr
$ github.com/tomwright/queryparam
$ go install erchallenge.go

7. a binary file called 'erchallenge' should now be created within your $GOBIN bin folder.
If not found run "go env" in your terminal (anywhere) to check your GOBIN is set. Also make sure the $GOBIN directory is added to your envrionment $PATH variable 

8. You can now run the following command to start the app:
$ erchallenge
This will launch the app which will listen on port 80.

The one and only end point available is /v1/compare-rates-euro{CURRENCY}  where {CURRENCY} is an upper case three letter currency acronym 

## Example use
GET request for url [link] http://your-host-name/uk/v1/compare-rates-euro/USD
Example response:

{
  "base_currency": "USD",
  "comparison_currency": "EUR",
  "price": 0.8877052818,
  "rate": "One USD is worth 0.89 EUR",
  "recommendation": "the price is rising, time to buy and ride that wave!"
}

USD and GBP are supported as well as some other currencies.

## How to run on a different port than 80
Follow the above steps up to and including step 5. Then use your favorite text editor to edit the file located at
bitbucket.org/droptec/exchange_rates_challenge/cmd/app/erchallenge.go
Change the port number on line 22. For example, to set the app to run on port 8080 you would edit line 22 so it looks like this

ListeningAddress = ":8080"

Save the file, then continue from step 6 onwards.

## Running tests
Within a terminal naviate to the /bitbucket.org/droptec/exchange_rates_challenge directory and run the following command:
$ go test ./...
This will run all the unit tests within the app
