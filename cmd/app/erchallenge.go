package main

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client"
	"bitbucket.org/droptec/exchange_rates_challenge/internal"
	"github.com/rs/zerolog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	logger              zerolog.Logger
	quitCh              chan os.Signal
	shutdownCh          chan struct{}
	exchangeRatesClient exchange_rates_client.Client
)

const (
	ListeningAddress = ":80"
)

func main() {
	logger = zerolog.
		New(os.Stdout).
		With().
		Str("service", "Exchange Comparison").
		Timestamp().
		Logger()

	// handle shutting down the program
	shutdownCh = make(chan struct{})
	go waitForShutdownSignal()


	// initialise the program
	httpClientWithTimeout := &http.Client{
		Timeout: time.Second * 30,
	}
	exchangeRatesClient = exchange_rates_client.NewClient("", httpClientWithTimeout)
	internal.Init(exchangeRatesClient, ListeningAddress, logger)
	// boot it up
	internal.Start()
}

func waitForShutdownSignal() {
	quitCh = make(chan os.Signal)
	signal.Notify(quitCh, os.Interrupt, syscall.SIGTERM)

	for {
		<-quitCh
		// Received shutdown signal
		os.Exit(0)
	}
}
