package exchange_rates_client

import (
	"encoding/json"
	"fmt"
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
	"time"
)

const DateLayOut = "2006-01-02"

func (client httpClient) GetRatesForPrevDays(days int, baseCurrency domain.Currency) (rates domain.Rates, err error) {
	date := time.Now().AddDate(0, 0, -days)
	url := fmt.Sprintf("%s/%s", client.baseURL, date.Format(DateLayOut))
	if baseCurrency != BaseCurrencyDefault {
		url = fmt.Sprintf("%s/%s?base=%s", client.baseURL, date.Format(DateLayOut), baseCurrency)
	}

	respBody, err := client.httpRequests.HttpGetRequest(url)
	if err != nil {
		return nil, fmt.Errorf("getLatestRates failed to make http request, err:%v", err.Error())
	}

	r := RatesResponse{}
	if err := json.Unmarshal(respBody, &r); err != nil {
		return nil, fmt.Errorf("getLatestRates failed unable to unmarshal response in to rates struct, err: %v", err.Error())
	}

	rates, err = MapRatesResponseToLocalRates(r)
	if err != nil {
		return nil, fmt.Errorf("could not map response rates to Rates, err:%v", err.Error())
	}

	return rates, nil
}
