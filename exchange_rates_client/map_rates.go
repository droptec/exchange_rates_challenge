package exchange_rates_client

import (
	"fmt"
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
)

func MapRatesResponseToLocalRates(response RatesResponse) (domain.Rates, error) {
	rates := make(domain.Rates, 0)
	for currencyStr, price := range response.Rates {
		currency, err := domain.GetCurrencyFromString(currencyStr)
		if err != nil {
			return nil, fmt.Errorf("MapRatesResponseToLocalRates found error with GetCurrencyFromString, err:%v ", err.Error())
		}
		rates[currency] = domain.Price(price)
	}

	return rates, nil
}
