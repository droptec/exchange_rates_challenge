package exchange_rates_client

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
	"net/http"
)

var (
	BaseUrlDefault      = "https://api.exchangeratesapi.io"
	BaseCurrencyDefault = domain.Currency("EUR")
)

type HttpRequests interface {
	HttpGetRequest(url string) (respBody []byte, err error)
	// we can add more requests here as we need them
}

type httpRequests struct {
	httpClient *http.Client
}

type httpClient struct {
	baseURL      string
	httpRequests HttpRequests
}

func NewClient(baseUrl string, httpClientCustom *http.Client) Client {
	if baseUrl == "" {
		baseUrl = BaseUrlDefault
	}
	httpRequests := httpRequests{
		httpClient: httpClientCustom,
	}

	return httpClient{
		baseURL:      baseUrl,
		httpRequests: httpRequests,
	}
}
