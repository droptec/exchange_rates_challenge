package exchange_rates_client

import (
	"encoding/json"
	"fmt"
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
)

const (
	LatestEndpoint = "latest"
)

func (client httpClient) GetLatestRates(baseCurrency domain.Currency) (rates domain.Rates, err error) {
	url := fmt.Sprintf("%s/%s", client.baseURL, LatestEndpoint)
	if baseCurrency != BaseCurrencyDefault {
		url = fmt.Sprintf("%s/%s?base=%s", client.baseURL, LatestEndpoint, baseCurrency)
	}
	respBody, err := client.httpRequests.HttpGetRequest(url)
	if err != nil {
		return nil, fmt.Errorf("getLatestRates failed to make http request, err:%v", err.Error())
	}

	r := RatesResponse{}
	if err := json.Unmarshal(respBody, &r); err != nil {
		return nil, fmt.Errorf("getLatestRates failed unable to unmarshal response in to rates struct, err: %v", err.Error())
	}

	rates, err = MapRatesResponseToLocalRates(r)
	if err != nil {
		return nil, fmt.Errorf("could not map response rates to Rates, err:%v", err.Error())
	}

	return rates, nil
}
