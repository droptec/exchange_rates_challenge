package domain

import "fmt"

type Rate map[Currency]Price

type Currency string

func GetCurrencyFromString(currencyStr string) (Currency, error) {
	switch currencyStr {
	case "EUR":
		return Currency("EUR"), nil
	case "BGN":
		return Currency("BGN"), nil
	case "NZD":
		return Currency("NZD"), nil
	case "ILS":
		return Currency("ILS"), nil
	case "RUB":
		return Currency("RUB"), nil
	case "CAD":
		return Currency("CAD"), nil
	case "USD":
		return Currency("USD"), nil
	case "PHP":
		return Currency("PHP"), nil
	case "CHF":
		return Currency("CHF"), nil
	case "ZAR":
		return Currency("ZAR"), nil
	case "AUD":
		return Currency("AUD"), nil
	case "JPY":
		return Currency("JPY"), nil
	case "TRY":
		return Currency("TRY"), nil
	case "HKD":
		return Currency("HKD"), nil
	case "MYR":
		return Currency("MYR"), nil
	case "THB":
		return Currency("THB"), nil
	case "HRK":
		return Currency("HRK"), nil
	case "NOK":
		return Currency("NOK"), nil
	case "IDR":
		return Currency("IDR"), nil
	case "DKK":
		return Currency("DKK"), nil
	case "CZK":
		return Currency("CZK"), nil
	case "HUF":
		return Currency("HUF"), nil
	case "GBP":
		return Currency("GBP"), nil
	case "MXN":
		return Currency("MXN"), nil
	case "KRW":
		return Currency("KRW"), nil
	case "ISK":
		return Currency("ISK"), nil
	case "SGD":
		return Currency("SGD"), nil
	case "BRL":
		return Currency("BRL"), nil
	case "PLN":
		return Currency("PLN"), nil
	case "INR":
		return Currency("INR"), nil
	case "RON":
		return Currency("RON"), nil
	case "CNY":
		return Currency("CNY"), nil
	case "SEK":
		return Currency("SEK"), nil

	}

	return Currency("N/A"), fmt.Errorf("could not parse currency string(%v) to a known Currency", currencyStr)
}
