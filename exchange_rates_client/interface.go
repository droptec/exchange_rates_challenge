package exchange_rates_client

import "bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"

type Client interface {
	GetLatestRates(baseCurrency domain.Currency) (domain.Rates, error)
	GetRatesForPrevDays(days int, baseCurrency domain.Currency) (domain.Rates, error)
}
