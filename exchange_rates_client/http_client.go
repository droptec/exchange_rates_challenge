package exchange_rates_client

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

// assumes 200 status response is expected
func (client httpRequests) HttpGetRequest(url string) (respBody []byte, err error) {
	r, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("could not create New GET Request, err:%v", err.Error())
	}

	resp, err := client.httpClient.Do(r)
	if err != nil {
		return nil, fmt.Errorf("could not Do http GET request, err: %v", err.Error())
	}

	if resp.Body != nil {
		respBody, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, fmt.Errorf("could not read body of response, err: %v", err.Error())
		}
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("received non 200 status code, err: %v", err.Error())
	}
	if respBody == nil {
		return nil, fmt.Errorf("received valid (200) but empty response, err: %v", err.Error())
	}

	return respBody, nil
}
