package exchange_rates_client

import (
	"bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"
	"testing"
)

func TestGetLatestRates(t *testing.T) {
	c := NewClientMock()
	rates, err := c.GetLatestRates(domain.Currency("EUR"))
	if err != nil {
		t.Errorf("error with GetLatestRates, err:%v", err.Error())
	}
	price, ok := rates["GBP"]
	if !ok {
		t.Errorf("could not find GBP in rates for prev days response")
	}
	if price != 0.88948 {
		t.Errorf("expected (%v) but fot (%v)", 0.88948, price)
	}
}
