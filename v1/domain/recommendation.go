package domain

import "bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"

type RecommendationData struct {
	PriceLastWeek domain.Price
	PriceCurrent  domain.Price
}
