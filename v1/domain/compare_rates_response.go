package domain

import "bitbucket.org/droptec/exchange_rates_challenge/exchange_rates_client/domain"

type CompareRatesResponse struct {
	BaseCurrency       domain.Currency `json:"base_currency"`
	ComparisonCurrency domain.Currency `json:"comparison_currency"`
	Price              domain.Price    `json:"price"`
	Rate               string          `json:"rate"`
	Recommendation     string          `json:"recommendation"`
}
